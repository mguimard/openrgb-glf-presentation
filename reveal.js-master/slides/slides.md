![](slides/logo.webp)

<p class="rb">Podcast GLF - Jeudi 07 Septembre 2023 - morg</p>

---

## Kikezé <span class="hl">morg</span> ?

* Breton
* Troll
* Papa
* Consultant SI
* Debian user

---

## Kézézé OpenRGB ?

https://openrgb.org/

* Solution logicielle
* Centralise le contrôle RGB
* Léger (quelques Mo)
* Elimination du bloatware
* Open Source (GPL v2) - Cross Platform https://gitlab.com/CalcProgrammer1/OpenRGB

---

## Matériels supportés

* Souris, claviers
* Cartes mères, RAM
* Carte graphiques
* Ampoules connectées
* Arduino, ESP, ...

Environ **2000** références

https://openrgb.org/devices.html

---

## Versions depuis 2 ans

Dernier podcast GLF :  Dec 8, 2021

* 0.7 (30 Décembre 2021)
* 0.8 (28 Novembre 2022)
* **0.9** (10 Juillet 2023)
* experimental (origin/master)

https://gitlab.com/CalcProgrammer1/OpenRGB/-/releases

---

![get-ready](slides/get-ready.jpg)
---

## OpenRGB

![overview](slides/overview.png)

---

## Cas d'usages, étendre OpenRGB

Protocoles **standards**, interfaçables.

HTTP, TCP, SDK ouverts...

* Changer le RGB en fonction de la **météo**, cours du **bitcoin**, cycle jour/nuit dans Warframe...
* Lancer un effet lors d'une chute dans **Minecraft**
* Controler l'éclairage d'une pièce depuis un **smartphone** (Android)

---

## Démos

* **GUI** : L'interface graphique
* **Scripting** : changer les couleurs en fonction de la météo
* Moteur d'**effets** : layers, shaders
* **Vidéos** d'utilisateurs

---

## Mes contributions

* OpenRGB : 163 commits
* Plugins : 794 commits

Contributions en tout genres, IHM, drivers, plugins, site web, doc, système de build...

Création d'outils pour aider à la rétro ingénierie

https://gitlab.com/mguimard/USBReverseHelper

---

## Ajouter un device USB

C'est vraiment si **compliqué** ?

---

![easy](slides/easy.jpg)

---

## Etape 1

Découverte des identifiants USB de l'appareil

Sous linux

```bash
lsusb
Bus 002 Device 006: ID 05ac:0253 Apple, Inc. Internal Keyboard/Trackpad (ISO)
Bus 002 Device 005: ID 05ac:8242 Apple, Inc. Built-in IR Receiver
Bus 002 Device 009: ID 05ac:821d Apple, Inc. Bluetooth USB Host Controller
Bus 002 Device 004: ID 0a5c:4500 Broadcom Corp. BCM2046B1 USB 2.0 Hub (part of BCM2046 Bluetooth)
[...]
```

vendorId : productId 

05ac : 0253

---

## Etape 2

Capturer de la donnée avec Wireshark et USBPcap

* Nécéssite **Windows** (une VM peut faire l'affaire)
* Nécéssite d'installer le **logiciel officiel** fourni avec le matériel
* Nécessite de savoir **quoi capturer**
* Nécessite de savoir cliquer sur les boutons **start**/**stop**/**save**

https://www.wireshark.org/download.html

---

## Mais dis-moi Jamy, Kézézé qu'il faut capturer ?

<div class="container">

<div class="col">

Le changement d'**un seul paramètre** par capture évidement ! 
<br/> <br/> Par exemple : faire varier la luminosité de 0 à 100%
<img src="slides/slider.png"/>

</div>

<div class="col">
<img src="slides/jamy-2.jpeg" />
</div>

</div>

---

## Analyse de la capture

**Bingo**, la colonne 4 d'un paquet contrôle la luminosité. Valeurs min/max : **0x00**, **0x32**

![capture](slides/capture.png)

<small>Chaque ligne est un paquet envoyé par l'application.</small>

---

## Et après ?

Faire une capture pour tous les autres paramètres

* Démarrage de l'appli officielle
* Débrancher/rebrancher le matériel
* Couleurs
* Modèle d'effet
* Vitesse d'effet
* Direction de l'effet
* etc...

---

![wohoo](slides/wohoo.jpeg)

---

## Retour à la réalité

Chiant mais indispensable : **documentation** du protocole une fois le puzzle résolu.

```txt
Byte    Meaning         Values
---------------------------------------------------
0       Report id       0x08
1       Mode selector   0x01, 0x02, ...
2       Direction       0x00 = left, 0x01 = right           
3       Speed           from 0x01 to 0x0A
4       Brightness      from 0x00 to 0x32
5       Color           0x00 = black, 0x01=red, ...
6       Save to flash   0x00 = no, 0x01 = yes
7       Constant        always 0x00 - stupid devs
```

---

## Ajout dans OpenRGB

2 cas s'offrent à nous.

* Soit le protocole est **déjà connu** dans OpenRGB -> On ajoute simplement les **identifiants** dans le code.
* Ou bien c'est un nouveau protocole -> On écrit le **code** pour **envoyer** correctement les **paquets** au matériel.

---

## C++ ~~is easy~~

Exemple simplifié d'**envoi** d'un **paquet** pour notre cas d'étude

```cpp
unsigned char packet[8];

packet[0] = 0x08;  // ReportId
packet[1] = 0x01;  // Mode
packet[2] = 0x00;  // Direction
packet[3] = 0x04;  // Speed
packet[4] = 0x32;  // Brightness
packet[5] = 0x03;  // Color
packet[6] = 0x00;  // Save
packet[7] = 0x00;  // Boring

hid_write(device, packet, 8);
```

---

## Le turfu

* Correction de bugs
* Hotplug system
* Toujours plus de devices
* Ajouts de langues (traductions)
* Ajouts d'effets

---

## Installation - officiel

Le core team propose en **téléchargement** :

* le **source** - t'es un grand tu compiles avec gcc
* .**deb** - t'es fénéant, apt install ./openrgb.deb
* .**rpm** - je sais pas, lis la doc de dnf
* **AppImage** - chmod +x
* Un bon gros .**exe** des familles
* Un .**dmg** pour les hipsters

---

## Installation - communauté

Certains users en dehors du core team proposent :

* flatpak - wtf is this...
* nix - hmmm, ouais cool
* ppa - ubuntu blablabla
* AUR - truc obscure

On les remercie pour ça, et on essaye au mieux d'appoter notre support 

---

## Merci

... de me payer une bière et un paquet de chips 

(:

---

![patrick](slides/patrick.jpg)
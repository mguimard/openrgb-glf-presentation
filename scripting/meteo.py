#!/usr/bin/env python3

# Sample python script that requests the current temperature
# for a given latitude/longitude and use this value to change
# OpenRGB devices color.

import sys
import requests
from openrgb import OpenRGBClient
from openrgb.utils import RGBColor, DeviceType

# Default location
default_location = {
    "lat": 48.8566,
    "lon": 2.3522
}

# Get lat/lon from command line arguments
try:
    lat = sys.argv[1]
except:
    lat = default_location["lat"]

try:
    lon = sys.argv[2]
except:
    lon = default_location["lon"]

# Request weather API
print("Requesting weather data...")
url='https://openweathermap.org/data/2.5/onecall?lat='+str(lat)+'&lon='+str(lon)+'&units=metric&appid=439d4b804bc8187953eb36d2a8c26a02'
r = requests.get(url)
current_temperature = r.json()["current"]["temp"]
print('Current temperature is ' + str(current_temperature))

# Connect OpenRGB and turn lights off
print("Connecting OpenRGB...")
client = OpenRGBClient()
client.clear()

color = RGBColor.fromHSV(current_temperature, 100, 100)

# Use temperature as color hue
print("Set color ...")
print(color)
for device in client.devices:
    device.set_color(color)
